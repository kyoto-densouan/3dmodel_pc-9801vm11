# README #

1/3スケールのNEC PC-9801VM11風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- NEC

## 発売時期
- 1988年11月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/PC-9800%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-9801vm11/raw/8e3165f0a225f7180c3f8721603b20678b61e68a/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-9801vm11/raw/8e3165f0a225f7180c3f8721603b20678b61e68a/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_pc-9801vm11/raw/8e3165f0a225f7180c3f8721603b20678b61e68a/ExampleImage.png)
